import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

/**
 * CodeConverterThread is the executing part of the CodeConverter program.
 * Each client connecting will create a new thread. The server has a fixed
 * limit of threads running at any time.
 *
 * SENG3400 Assignment 1
 * @author Christian Lassen
 * Student  3121707
 */
class CodeConverterThread implements Runnable {
    private Socket clientSocket;
    private ConverterProtocol cProtocol;
    private String hostname;
    private int portNumber;

    public CodeConverterThread(Socket s) {
        cProtocol = new ConverterProtocol();
        clientSocket = s;
        portNumber = s.getLocalPort();
        hostname = s.getInetAddress().getHostName();
    }

    /**
     * The executing method for the CodeConverterThread class.
     * Loops as long as the inputstream from client is active.
     */
    public void run() {
        // Establish connection
        try (
                PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
                BufferedReader in = new BufferedReader(new InputStreamReader((clientSocket.getInputStream())))
        ) {

            System.out.println(hostname + ":" + portNumber + " connected.");

            String clientCommand;
            String outputLine;
            boolean activeSession = false;

            // Client must input "ASCII" to initiate a session
            // An invalid input is communicated back to the client
            while (!activeSession) {
                clientCommand = in.readLine();
                System.out.println("CLIENT: " + clientCommand);

                if (clientCommand.equals("ASCII")) {
                    out.println("ASCII: OK");
                    System.out.println("SERVER: ASCII: OK");
                    activeSession = true;
                } else {
                    out.println("Invalid response from server. Closing connection");
                    System.out.println("Invalid message from client: " + clientCommand);
                }
            }

            mainLoop:
            while ((clientCommand = in.readLine()) != null) {

                //Clear outputline
                outputLine = "";

                System.out.println("CLIENT: " + clientCommand);
                switch (clientCommand) {
                    case "AC":
                        cProtocol.changeState(ConverterProtocol.conversionState.AC);
                        outputLine += "CHANGE: OK";
                        break;

                    case "CA":
                        cProtocol.changeState(ConverterProtocol.conversionState.CA);
                        outputLine += "CHANGE: OK";
                        break;

                    // Upon receiving a "BYE" request, break main loop
                    case "BYE":
                        out.println("BYE: OK");
                        break mainLoop;

                    // Upon receiving an "END" request from a client, close socket and all streams,
                    // call the function to kill server
                    case "END":
                        out.println("END: OK");
                        out.close();
                        in.close();
                        clientSocket.close();
                        CodeConverter.shutDownServer(); // Begins shutting down threads and kill server
                        System.exit(1);

                    // No operational commands were input, run conversion protocol on input
                    default:
                        outputLine += (cProtocol.processInput(clientCommand));
                        break;
                }

                // Print result of conversion to client.
                out.println(outputLine);
                out.println("ASCII: OK");

                // Print result to server terminal
                System.out.println("SERVER: " + outputLine);
                System.out.println("SERVER: ASCII: OK");
                out.flush();

            }

            // Clean up and print disconnect message
            clientSocket.close();
            System.out.println(hostname + ":" + portNumber + " disconnected.");

        } catch (IOException e) {
            System.out.println("Exception caught when attempting to listen to " + hostname + "on port " + portNumber);
            System.out.println(e.getMessage());
        }
    }
}