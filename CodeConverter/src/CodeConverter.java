import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * CodeConverter, a program which will attempt to open a connection with a
 * client on a socket. The server take requests from user client and perform
 * tasks, then return the result of the request. All tasks are carried out
 * with commands in a terminal.
 *
 * SENG3400 Assignment 1
 * @author  Christian Lassen
 * Student  3121707
 */
public class CodeConverter {
    private static final int MAX_THREADS_IN_POOL = 5;   // Maximum threads in executor service
    private static ServerSocket serverSocket;           // Server-side socket
    private static Socket clientSocket;                 // Client-side socket
    private static ExecutorService executor;            // A service which maintains the thread-pool

    /**
     * Constructor for CodeConverter server class.
     * @param portNumber The port number for the server
     * @throws IOException upon failure to establish a connection with client
     */
    public CodeConverter(int portNumber) throws IOException {
        serverSocket = new ServerSocket(portNumber);
    }

    /**
     * Main method for CodeConverter server.
     * Attempts to parse an integer port number from supplied run argument.
     * Generates an executor with a fixed thread pool of 10.
     * Upon shutdown, will kill all threads before exiting.
     *
     * @param args Takes either 0 or 1 arguments. At 0 arguments, the server uses the standard
     *             port; 12345. Else, it uses the port number supplied here.
     */
    public static void main(String[] args) {
        int portNumber = 12345;
        // Localhost is specified, use default port
        if (args.length == 1) {
            portNumber = Integer.parseInt(args[0]);
            // Check for illegal ports.
            // Ports lower then 1023 are reserved for system resources
            if(portNumber <= 1023 || portNumber > 65535) {
                System.out.println("Illegal port number. Must be higher than 1023 and less than 65535");
                System.out.println("Program will now exit.");
                System.exit(1);
            }
        }
        // More than 1 parameters; invalid parameters
        else if (args.length > 1) {
            System.out.println("Please run program with 0 or 1 parameter:");
            System.out.println("java CodeConverter [port]");
            System.exit(1);
        }

        try {
            serverSocket = new ServerSocket(portNumber);
        } catch(IOException ex) {
            System.out.println(ex);
        }

        System.out.println("Server started, waiting for client connection");

        // ExecutorService for thread pool management
        executor = Executors.newFixedThreadPool(MAX_THREADS_IN_POOL);

        // Listen for connections and accept all
        while(true) {
            try {
                clientSocket = serverSocket.accept();
            } catch (IOException ex) {
                ex.printStackTrace();
            }

            // Start thread
            Thread clientThread = new Thread(new CodeConverterThread(clientSocket));
            executor.submit(clientThread);
        }
    }

    /**
     * Method for shutting down the server.
     * Calls Executor to kill all threads and exits the process with exit code 1.
     */
    public static void shutDownServer() {
        executor.shutdownNow();
        System.exit(1);
    }
}
