/**
 * CodeConverterProtocol. This class caries out all the tasks performed by the program.
 * A new instance of ConverterProtocol is created for every CodeConverterThread.
 * A state is maintained with a Java enum object which is public.
 *
 * SENG3400 Assignment 1
 * @author  Christian Lassen
 * Student  3121707
 */
public class ConverterProtocol {

    public enum conversionState {AC, CA}
    private conversionState state;

    public ConverterProtocol() {
        state = conversionState.CA;
    }

    /**
     * This method performs THREE tasks
     *      1) Calls verifyInput to determine if the input is valid
     *      2) Calls the appropriate method determined by the current protocol state
     *      3) Returns a) an error if invalid input was provided or b) the resulting
     *      converted string.
     * @param input String input from client
     * @return String An error message or the result of a correctly converted string
     */
    public String processInput(String input) {

        // Verify that the user input is valid, else return error message.
        if(!verifyInput(input))
            return "ERR";

        String result = "";

        // Switch on the current state
        switch(state) {
                case CA:
                    result += CA(input.charAt(0)); // verifyInput has already check the length of input
                    break;
                case AC:
                    int parsedNum = Integer.parseInt(input); // verifyInput has already verified input.
                    result += AC(parsedNum);
                    break;
        }
        return result;
    }

    /**
     * Method for changing between the states of the ConverterProtocol.
     * Protocol has two states:
     *      AC: ASCII code to Character.
     *      CA: Character to ASCII code
     * @param cs This is the new state for the protocol instance.
     */
    public void changeState(conversionState cs) {
        state = cs;
    }

    /**
     * This method return the ASCII code for the character provided as input.
     * @param c An alphabetical letter (a-z, A-Z) or a digit (0-9)
     * @return int The ASCII code for the character provided from client
     */
    private int CA(char c) {
        return c;
    }

    /**
     * This method returns the character which corresponds to the ASCII code number
     * provided by the client input
     * @param i The ASCII code to convert to character
     * @return char The character which corresponds to the ASCII code provided
     */
    private char AC(int i) {
        return (char)i;
    }

    /**
     * This method takes the input from client and verifies that the input is valid.
     *  In AC mode, method returns true if input is a non-negative integer within the
     *  ranges 48-57, 65-90, 97-122. False if otherwise.
     *
     *  In CA mode, method return true is
     *      1) input is only ONE character long AND
     *      2) said character is an alphabetical letter (A-Z, a-z) or a (digit 0-9)
     * @param input The input from the client which will be verified
     * @return boolean representing whether or not the input was valid
     */
    private boolean verifyInput(String input) {
        boolean validInput = false;

        // Switch on state, check properties
        switch(state) {
            case CA:
                // Character to ASCII
                // Returns true if the character is a letter or a digit
                if  (input.length() == 1 && input.matches("^[a-zA-Z0-9]+$")) // Regex matches upper and lower-case
                    validInput = true;                                       // letters and as digits
                break;
            case AC:
                if(input.matches("^[1-9]\\d*$")){ // Checks for positive integers without leading zero(s)
                    int parsedNum = Integer.parseInt(input);
                    if  (
                        (parsedNum >= 65 && parsedNum <=90) ||      // Upper case letter A-Z
                        (parsedNum >= 97 && parsedNum <= 122) ||    // Lower case letter a-z
                        (parsedNum >= 48 && parsedNum <= 57)        // Digits 0-9
                        )
                        validInput = true;
                }
                break;
        }
        return validInput;
    }
}
