import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

/**
 * CodeClient, a program which will attempt to open a connection with a
 * server on a socket. The client will let a user communicate with a server
 * which will perform various tasks. All tasks are carried out with commands
 * in a terminal.
 *
 * SENG3400 Assignment 1
 * @author  Christian Lassen
 * Student  3121707
 */
class CodeClient {

    private static String hostname;
    private static int portNumber;
    private final Scanner keyboardScanner;

    private void run() {
        try (
                // Open connection on hostname and port number
                // Declare and Initialise in and out streams
                Socket serverSocket = new Socket(hostname, portNumber);
                PrintWriter out = new PrintWriter(serverSocket.getOutputStream(), true);
                BufferedReader in = new BufferedReader((new InputStreamReader(serverSocket.getInputStream())))
        ) {
            System.out.println("Connected to " + hostname + " on port " + portNumber);

            String echo;            // The output from the server
            String echo2;           // Second line output from the server
            String clientCommand;   // The input from client to server

            out.println("ASCII");   // This was hard-coded in the sample program so I did the same.
            System.out.println("CLIENT: ASCII");
            System.out.println("SERVER: " + in.readLine());

            while (serverSocket.isConnected()) {

                // Take user input
                System.out.println("Enter command to send to server [AC,CA,BYE,END, or something to convert]:");
                clientCommand = keyboardScanner.nextLine();
                if (clientCommand != null) {
                    System.out.println("CLIENT: " + clientCommand);
                    out.println(clientCommand);
                }

                echo = in.readLine();
                System.out.println("SERVER: " + echo);

                // Close current session and exit if server echo's "BYE: OK" or "END: OK
                // Close socket and all streams
                if (echo.contains("BYE: OK") || echo.contains("END: OK")) {
                    out.close();
                    in.close();
                    serverSocket.close();
                    System.exit(1);
                }

                // Continue reading output from server
                echo2 = in.readLine();
                System.out.println("SERVER: " + echo2);

            }

            // Show disconnect
            System.out.println("Disconnected from " + hostname);

        } catch (UnknownHostException e) {
            System.out.println("Unable to connect to host " + hostname);
            System.exit(0);
        } catch (IOException e) {
            System.out.println("Could not get I/O for connection on host " + hostname);
            System.exit(0);
        }
    }

    private CodeClient(String host, int port) {
        keyboardScanner = new Scanner(System.in);
        hostname = host;
        portNumber = port;
    }

    public static void main(String[] args) {
        hostname = "localhost";
        portNumber = 12345;
        // Localhost is not specified, use default port
        if (args.length == 1) {
            portNumber = Integer.parseInt(args[0]);

            // Check for illegal port used
            if(portNumber <= 1023 || portNumber > 65536) {
                System.out.println("Illegal port number. Must be higher than 1023 and at most 65536");
                System.exit(1);
            }
        }
        // Localhost and port is specified
        else if (args.length == 2) {
            portNumber = Integer.parseInt(args[0]);
            hostname = args[1];

            // Check for illegal port used
            if(portNumber <= 1023 || portNumber > 65536) {
                System.out.println("Illegal port number. Must be higher than 1023 and at most 65536");
                System.exit(1);
            }
        }
        // More than 2 parameters; invalid parameters
        else if (args.length > 2) {
            System.out.println("Please run program with 0, 1 or 2 parameter:");
            System.out.println("java TaxServer [port] [hostname]");
        }

        CodeClient client = new CodeClient(hostname, portNumber);
        client.run();
    }
}