# SENG3400 Assignment 1 #

Introduction to Client-Server network applications using the [Java 7 Socket API](https://docs.oracle.com/javase/7/docs/api/java/net/Socket.html).
The function of the program is to allow clients to connect and perform simple commands (convert ASCII letter to integer to integer and vice versa).

## To run ##

1: Compile

```
#!console

javac *.java
```

2: Run server

```
#!console

java CodeConverter <port>
```
port number is optional. Defaults to 12345

3: Run client

```
#!console

java CodeClient <port> <hostname>
```
port number and hostname is optional, however host name must accomopany a port number
port number defaults to 12345. Host name defaults to "localhost"


## Usage (case sensitive): ##

```
#!console

CA
```

Instructs the server to switch to Character to ASCII code conversion state.

```
#!console

AC
```

Instructs the server to switch to ASCII code to character conversion state.

```
#!console

BYE
```

Communicates to the server to end the current session. Ends client.

```
#!console

END
```

Communicates to the server to shut down.